package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class AuthFilter
 */
@WebFilter("/*")
public class AuthFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public AuthFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		HttpSession session = req.getSession();
		String uri = req.getRequestURI();
		List<String> uris = new ArrayList<>();
		uris.add("/UserDeleteServlet");
		uris.add("/UserDetailServlet");
		uris.add("/UserEditServlet");
		uris.add("/UserManageServlet");
		uris.add("/UserRegistServlet");
		uris.add("/userRegist.jsp");
		uris.add("/userDeleteComp.jsp");
		uris.add("/userDetail.jsp");
		uris.add("/userEdit.jsp");
		uris.add("/userEditComp.jsp");
		uris.add("/userManage.jsp");
		uris.add("/userRegistComp.jsp");
		uris.add("/eventEditAdmin.jsp");
		if (!uri.endsWith("/LoginServlet")) {
			if (session.getAttribute("loginId") == null) {
				res.sendRedirect("LoginServlet");
				return;
			} else {

				for (String i : uris) {
					if (uri.endsWith(i)) {
						if ((Integer) session.getAttribute("loginUserType") != 1) {
							res.sendRedirect("TodaysEventServlet");
							return;
						}
					}
				}

			}
		} else {
			if (session.getAttribute("loginId") != null) {
				res.sendRedirect("TodaysEventServlet");
				return;
			}
		}

		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
