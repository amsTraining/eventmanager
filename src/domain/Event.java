package domain;

import java.util.Date;

public class Event {
	private Integer eventId;
	//	テーブルeventsのidです。
	private String title;
	//	イベント名です。
	private Date start;
	//	イベントの開始時間です。
	private Date end;
	//	イベントの終わり時間です。
	private String place;
	//	イベントが行われる場所です。
	private Integer groupId;

	private Integer pageCount;

	private Integer row;
	//		行数

	private Integer userId;

	private String loginId;

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	private Boolean attendtf;

	public Boolean getAttendtf() {
		return attendtf;
	}

	public void setAttendtf(Boolean attendtf) {
		this.attendtf = attendtf;
	}

	public Integer getRow() {
		return row;
	}

	public void setRow(Integer row) {
		this.row = row;
	}

	public Integer getPageCount() {
		return pageCount;
	}

	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	private String detail;
	//	イベントの詳細です。
	private Integer registeredById;
	//イベントを登録したユーザのIDです。
	private String registeredByName;
	//	イベントを登録したユーザの名前です。
	private String attendName;
	//参加者を入れる配列です。
	private Date eventCreated;
	//	イベントが登録された時間です。
	private String groupName;

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getAttendName() {
		return attendName;
	}

	public void setAttendName(String attendName) {
		this.attendName = attendName;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Integer getRegisteredById() {
		return registeredById;
	}

	public void setRegisteredById(Integer registeredById) {
		this.registeredById = registeredById;
	}

	public String getRegisteredByName() {
		return registeredByName;
	}

	public void setRegisteredByName(String registeredByName) {
		this.registeredByName = registeredByName;
	}

	public Date getEventCreated() {
		return eventCreated;
	}

	public void setEventCreated(Date eventCreated) {
		this.eventCreated = eventCreated;
	}

}
