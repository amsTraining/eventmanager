package domain;

import java.util.Date;

public class User {

	private Integer userId;
	//	テーブルusersのidです。
	private String name;
	//	ユーザの名前です
	private Integer typeId;
	//	ユーザのtype_idです。権限関連のものなので、無視して構わないです。

	private Integer groupId;
	//	ユーザのgroup_idです
	private String groupName;
	//	groupsのname、ユーザのグループを表します。
	private Date userCreated;
	//	usersのcreatedです。ユーザ情報の作成時間を表します。
	private String pass;
	// ユーザーのpassです。
	private Integer pageCount;
	private Integer row;
	private String login_id;

	/**
	* @return login_id
	*/
	public String getLogin_id() {
		return login_id;
	}

	/**
	 * @param login_id セットする login_id
	 */
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	/**
	 * @return pageCount
	 */
	public Integer getPageCount() {
		return pageCount;
	}

	/**
	 * @param pageCount セットする pageCount
	 */
	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	/**
	 * @return row
	 */
	public Integer getRow() {
		return row;
	}

	/**
	 * @param row セットする row
	 */
	public void setRow(Integer row) {
		this.row = row;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Date getUserCreated() {
		return userCreated;
	}

	public void setUserCreated(Date userCreated) {
		this.userCreated = userCreated;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

}
