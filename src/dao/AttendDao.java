package dao;

import java.util.List;

import domain.Attend;
import domain.Event;

public interface AttendDao {
	public void insert(Attend attend) throws Exception;
	//イベントに参加します。

	public void delete(Attend attend) throws Exception;
	//イベントの参加を辞めます。

	public List<Attend> findByUserId(Attend attend) throws Exception;

	public List<Attend> findByLoginId(Attend attend) throws Exception;

	public List<Attend> findByEventListId(Integer user_id) throws Exception;

	public Attend ifAttendByEventId(List<Attend> attend) throws Exception;

	public boolean joining(Attend attend) throws Exception;

	public void attendDelete(Attend attend) throws Exception;

	public void attendDeleteEventId(Attend attend) throws Exception;

	public boolean ifAttendGroup(Event event) throws Exception;

	public void attendDeleteUserId(Attend attend) throws Exception;
}
