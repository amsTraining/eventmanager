package dao;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
* The copyright to the computer program(s) herein is the property of AMS Create,inc.
*  Chapter05.データベースプログラミング
*  Section11.ユーザ認証
*
*
*  練習11 ユーザ認証練習
 *
 *  DaoFactoryクラスです
 *  DaoFactoryクラス内では、DAOクラスのインスタンスを生成する際にDataSourceを取得し
 *  Daoクラスのコンストラクタへ渡します
 *
 *
 * @author trainee
 * @version 1.0
 */
public class DaoFactory {
	public static UserDao createUserDao() {
		return new UserDaoImpl(getDataSource());
	}

	public static LoginDao createLoginDao() {
		return new LoginDaoImpl(getDataSource());
	}

	public static EventDao createEventDao() {
		return new EventDaoImpl(getDataSource());
	}

	public static AttendDao createAttendDao() {
		return new AttendDaoImpl(getDataSource());
	}

	private static DataSource getDataSource() {

		InitialContext ctx = null;
		DataSource ds = null;
		try {
			ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/olddb");
		} catch (NamingException e) {
			if (ctx != null) {
				try {
					ctx.close();
				} catch (NamingException el) {
					throw new RuntimeException(el);
				}

			}
			throw new RuntimeException(e);
		}
		return ds;

	}

}
