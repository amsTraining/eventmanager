package dao;

import java.util.List;

import domain.User;

public interface UserDao {

	public List<User> findAll(User user) throws Exception;
	//	ユーザの一覧を表示させます。

	public void insert(User user) throws Exception;
	//	ユーザ情報を登録します。

	public void update(User user) throws Exception;
	//	ユーザ情報を編集します。

	public void delete(int id) throws Exception;
	//	ユーザ情報を削除します。

	public User findByUserId(User user) throws Exception;
	//ユーザの詳細を表示させます。

	public User findByUserId(Integer userId) throws Exception;

	public User findByGroupId(User group) throws Exception;

	public String findName(String loginId) throws Exception;

	public User userD(Integer user) throws Exception;

	public String makePasswordHash(String pass) throws Exception;

	public User Count() throws Exception;

	public User findByType_id(String login_id) throws Exception;

	public List<User> listLogin_id(Integer userId) throws Exception;

	public List<User> listLogin_id() throws Exception;
}
