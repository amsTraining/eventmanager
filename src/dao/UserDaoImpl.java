package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.mindrot.jbcrypt.BCrypt;

import domain.User;

public class UserDaoImpl implements UserDao {

	private DataSource ds;

	public UserDaoImpl(DataSource ds) {
		this.ds = ds;
	}

	public List<User> findAll(int startData) throws Exception {
		// TODO 自動生成されたメソッド・スタブ]

		//ユーザーを５件表示する
		try (Connection con = ds.getConnection()) {
			List<User> userList = new ArrayList<>();

			String sql = "Select users.login_id, users.name, groups,name from users join"
					+ " groups on users.group_id = groups.id Limit ?, 5 order by desc";
			//ユーザーの始まりを取得後、実際の件数を代入
			int startData_n = ((startData - 1) * 5);

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, startData_n, Types.INTEGER);
			ResultSet rs = stmt.executeQuery();

			//userListにSql文の結果を代入
			while (rs.next()) {
				userList.add(mapToEvent(rs));
			}
			return userList;
		}
	}

	public List<User> findAll(User user) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		Integer pg = 0;
		if (user.getPageCount() != null) {
			pg = pg + (user.getPageCount() - 1) * 5;
		}

		List<User> userList = new ArrayList<>();
		try (Connection con = ds.getConnection()) {
			String sql = "Select users.id, users.login_id, users.name, groups.name from users join groups on users.group_id = groups.id order by users.id desc Limit ?, 5";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, pg, Types.INTEGER);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				userList.add(mapToUserA(rs));
			}
		}

		return userList;
	}

	@Override
	public void insert(User user) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		try (Connection con = ds.getConnection()) {
			String sql = "Insert Into users (login_id, name, login_pass,type_id, group_id ,created) Values (?,?,?,?,?,now())";

			PreparedStatement stmt = con.prepareStatement(sql);

			//ここから?の部分の代入処理
			//ユーザーIDをセット
			stmt.setString(1, user.getLogin_id());
			//ユーザー名前をセット
			stmt.setString(2, user.getName());
			//ユーザーパスワードをセット
			stmt.setString(3, user.getPass());
			//ユーザー権限
			stmt.setObject(4, user.getTypeId());
			//ただしdomain/users.javaにパス用変数とセッターゲッター
			//グループIDをセット
			stmt.setObject(5, user.getGroupId(), Types.INTEGER);

			//ここまで
			stmt.executeUpdate();
		}
	}

	@Override
	public void update(User user) throws Exception {
		// TODO 自動生成されたメソッド・スタブ

		//編集管理
		try (Connection con = ds.getConnection()) {
			String sql = "Update users Set users.name = ?, users.type_id = ?, users.login_id=?, users.group_id=?";
			//ベースになるSQL文

			//パスワードが編集された場合はパスワード用のSQL文を追加
			//|| !user.getPass().isEmpty()
			if (user.getPass() != null) {

				//文字を結合するためのStringBuilderを設定
				StringBuilder buf = new StringBuilder();
				//Sql文をbufに代入
				buf.append(sql);
				//ベースのSql文に↑の文字を結合
				buf.append(", users.login_pass = ? where users.id = ?");
				//結合した結果をsql文に代入
				sql = buf.toString();

				PreparedStatement stmt = con.prepareStatement(sql);
				//stmtで追加した文字の?に代入
				stmt = con.prepareStatement(sql);
				//ユーザー名をセット
				stmt.setString(1, user.getName());
				//ユーザー権限をセット
				stmt.setObject(2, user.getTypeId());
				//ユーザーIDをセット
				stmt.setObject(3, user.getLogin_id());
				//グループIDをセット
				stmt.setObject(4, user.getGroupId());
				//パスワードをセット
				stmt.setString(5, user.getPass());
				//
				stmt.setObject(6, user.getUserId());
				stmt.executeUpdate();

			} else {

				StringBuilder buf = new StringBuilder();
				//Sql文をbufに代入
				buf.append(sql);
				//ベースのSql文に↑の文字を結合
				buf.append(" where users.id = ?");
				//結合した結果をsql文に代入
				sql = buf.toString();

				PreparedStatement stmt = con.prepareStatement(sql);
				//ユーザー名をセット
				stmt.setString(1, user.getName());
				//ユーザー権限をセット
				stmt.setObject(2, user.getTypeId());
				//ユーザーIDをセット
				stmt.setObject(3, user.getLogin_id());
				//グループIDをセット
				stmt.setObject(4, user.getGroupId());

				stmt.setObject(5, user.getUserId());

				stmt.executeUpdate();
			}

		}
	}

	@Override
	public void delete(int id) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		//削除の処理
		try (Connection con = ds.getConnection()) {
			String sql = "DELETE FROM users"
					+ " WHERE id = ?";
			PreparedStatement stmt = con.prepareStatement(sql);
			//ユーザーIDをセット
			stmt.setObject(1, id, Types.INTEGER);
			stmt.executeUpdate();
		}
	}

	@Override
	//ユーザーIDを通して対象データを見つける
	public User findByUserId(User user) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		User user_f = new User();
		try (Connection con = ds.getConnection()) {
			String sql = "SELECT* FROM USERS INNER JOIN GROUPS"
					+ " ON USERS.GROUP_ID = GROUPS.ID WHERE USERS.ID=?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setObject(1, user.getUserId(), Types.INTEGER);
			ResultSet rs = stmt.executeQuery();

			if (rs.next()) {
				user_f.setUserId((Integer) rs.getObject("users.id"));
				user_f.setName(rs.getString("users.name"));
				user_f.setLogin_id(rs.getString("users.login_id"));
				user_f.setPass(rs.getString("users.login_pass"));
				user_f.setGroupName(rs.getString("groups.name"));
				user_f.setTypeId((Integer) rs.getObject("users.type_id"));
			}
		}
		return user_f;
	}

	private User mapToEvent(ResultSet rs) throws SQLException {
		User user = new User();
		user.setLogin_id(rs.getString("login_id"));
		user.setName(rs.getString("name"));
		user.setGroupName(rs.getString("groups.name"));

		return user;
	}

	private User mapToUserA(ResultSet rs) throws SQLException {
		User user = new User();

		user.setUserId((Integer) rs.getObject("id"));
		user.setName(rs.getString("name"));

		user.setGroupName(rs.getString("groups.name"));
		return user;
	}

	@Override
	//ユーザーIDを通して対象データを見つける
	public User findByUserId(Integer userId) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		User user = new User();
		try (Connection con = ds.getConnection()) {
			String sql = "SELECT* FROM USERS INNER JOIN GROUPS"
					+ " ON USERS.GROUP_ID = GROUPS.ID WHERE USERS.ID=" + userId;
			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			if (rs.next()) {
				user.setUserId((Integer) rs.getObject("users.id"));
				user.setName(rs.getString("users.name"));
				user.setLogin_id(rs.getString("users.login_id"));
				user.setPass(rs.getString("users.login_pass"));
				user.setGroupName(rs.getString("groups.name"));
				user.setTypeId((Integer) rs.getObject("type_id"));
			}
		}
		return user;
	}

	@Override
	//グループIDを通して対象データを見つける
	public User findByGroupId(User group) throws Exception {
		User userGN = new User();
		try (Connection con = ds.getConnection()) {
			String sql = "select id from groups where name=?";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, group.getGroupName());
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				userGN.setGroupId((Integer) rs.getObject("groups.id"));
			}
		}
		return userGN;
	}

	private User mapToUser(ResultSet rs) throws SQLException {
		User user = new User();
		user.setUserId((Integer) rs.getObject("users.id"));
		user.setName(rs.getString("name"));
		user.setGroupName(rs.getString("group_name"));
		user.setTypeId((Integer) rs.getObject("type_id"));

		return user;
	}

	@Override
	//名前を通して対象データを見つける
	public String findName(String loginId) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		String username = "名無し";
		try (Connection con = ds.getConnection()) {
			String sql = "Select users.name from users where login_id = ?";
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, loginId);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				username = rs.getString("users.name");
			}
		}
		return username;
	}

	@Override
	public User userD(Integer user) throws Exception {
		User user2 = null;

		try (Connection conn = ds.getConnection()) {
			String sql = "SELECT"
					+ " users.id, users.name, users.type_id,users.group_id,"
					+ " groups.name as group_name"
					+ " from users join groups"
					+ " on users.group_id=groups.id WHERE users.id =? ;";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setObject(1, user, Types.INTEGER);
			ResultSet rs = stmt.executeQuery();
			rs.next();
			user2 = mapToUser(rs);
		}

		return user2;
	}

	@Override
	//多分ページカウント
	public User Count() throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		User userCo = new User();
		try (Connection con = ds.getConnection()) {
			String sql = "select * from users join groups " +
					"on users.group_id = groups.id ";
			PreparedStatement stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			rs.last();
			int row = rs.getRow();
			userCo.setRow(row);
			rs.beforeFirst();

		}
		return userCo;

	}

	//受け取ったパスワード文字列をハッシュ化するメソッド
	public String makePasswordHash(String pass) throws Exception {

		//String型に代入するgetParameterをしたときにこのメソッドを呼んでください
		//受け取ったパスワードをハッシュ化する
		String hashed = BCrypt.hashpw(pass, BCrypt.gensalt());
		return hashed;

	}

	@Override
	//対象ログインIDからユーザー権限を見つける
	public User findByType_id(String login_id) throws Exception {
		// TODO 自動生成されたメソッド・スタブ
		User type_get = new User();
		try (Connection conn = ds.getConnection()) {
			String sql = "Select type_id from users where users.login_id = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, login_id);
			ResultSet rs = stmt.executeQuery();
			rs.next();
			type_get.setTypeId((Integer) rs.getObject("type_id"));
		}

		return type_get;
	}

	public List<User> listLogin_id(Integer userId) throws Exception {

		List<User> listLogin_id = new ArrayList<>();
		try (Connection conn = ds.getConnection()) {
			String sql = "Select users.login_id from users where users.id not in (?)";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setObject(1, userId);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				User login_id = new User();
				login_id.setLogin_id(rs.getString("login_id"));
				listLogin_id.add(login_id);
			}
		}

		return listLogin_id;
	}

	public List<User> listLogin_id() throws Exception {

		List<User> listLogin_id = new ArrayList<>();
		try (Connection conn = ds.getConnection()) {
			String sql = "Select users.login_id from users";
			PreparedStatement stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				User login_id = new User();
				login_id.setLogin_id(rs.getString("login_id"));
				listLogin_id.add(login_id);
			}
		}

		return listLogin_id;
	}

}
