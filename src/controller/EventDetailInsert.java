package controller;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AttendDao;
import dao.DaoFactory;
import dao.EventDao;
import domain.Attend;
import domain.Event;

/**
 * Servlet implementation class EventDetailInsert
 */
@WebServlet("/EventDetailInsert")
public class EventDetailInsert extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EventDetailInsert() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//		int eventId = Integer.parseInt(request.getParameter("eventId"));
		//		int userId = 1;
		Integer userId = (Integer) request.getSession().getAttribute("loginUserId");

		Integer eventId = Integer.parseInt(request.getParameter("eventId"));
		//		int type_id = 1;
		boolean join = false;
		try {
			EventDao eventDao = DaoFactory.createEventDao();
			AttendDao attendDao = DaoFactory.createAttendDao();
			Event event = new Event();
			Attend attend = new Attend();

			event.setEventId(eventId);
			attend.setAttendEventId(eventId);
			attend.setAtttendUserId(userId);

			attendDao.insert(attend);

			List<Event> eventList = eventDao.findAttendName(event);
			event = eventDao.findByEventId(event);
			join = attendDao.joining(attend);
			Calendar now = Calendar.getInstance();
			boolean ifbefore = !event.getStart().before(now.getTime());

			request.setAttribute("ifbefore", ifbefore);
			request.setAttribute("event", event);
			request.setAttribute("eventList", eventList);
			request.setAttribute("join", join);
			//			request.setAttribute("type_id", type_id);
			request.setAttribute("userId", userId);
			request.setAttribute("eventId", eventId);

			request.getRequestDispatcher("WEB-INF/view/eventDetail.jsp").forward(request, response);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

}
