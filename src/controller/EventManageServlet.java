package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AttendDao;
import dao.DaoFactory;
import dao.EventDao;
import domain.Attend;
import domain.Event;

/**
 * Servlet implementation class EventManageServlet
 */
@WebServlet("/EventManageServlet")
public class EventManageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */

	//ログインして必ず表示される1ページ目用のサーブレット
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String id = null;
		String pg = null;

		try {

			AttendDao AttendDao = DaoFactory.createAttendDao();
			//			String loginUserId = (String) request.getSession().getAttribute("loginId");
			Attend a = new Attend();
			String loginId = (String) request.getSession().getAttribute("loginId");
			a.setLoginId(loginId);
			List<Attend> attendEventList = AttendDao.findByLoginId(a);
			EventDao EventDao = DaoFactory.createEventDao();

			if (request.getParameter("id") != null & request.getParameter("pg") != null) {
				id = request.getParameter("id");
				pg = request.getParameter("pg");
				Event event = new Event();
				event.setPageCount(Integer.parseInt(pg));
				List<Event> EventList = EventDao.findAll(event, attendEventList);

				request.setAttribute("EventList", EventList);
				request.getRequestDispatcher("WEB-INF/view/eventManage.jsp?id=" + id + "&page=" + pg).forward(request,
						response);

			} else {
				Event event = EventDao.Count();//最後の行を取得
				Integer r = event.getRow();//行数を取得
				int f = r % 5;//行数を5で割った余り
				Integer p = 1;//ページ番号
				if (r < 6) {//5行までは1ページ目
					p = 1;

				} else if (f != 0) {
					p = (r - r % 5) / 5 + 1;//余りが出たとき、振り分けられるページ番号

				} else {
					p = r / 5;//余りがないときは行数÷5がページ番号

				}
				event.setPageCount(1);
				List<Event> EventList = EventDao.findAll(event, attendEventList);

				request.setAttribute("EventList", EventList);
				request.getRequestDispatcher("WEB-INF/view/eventManage.jsp?id=" + p + "&page=1").forward(request, response);
			}

		} catch (Exception e) {
			throw new ServletException(e);
		}

	}
}
