package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import dao.UserDao;
import domain.User;

/**
 * Servlet implementation class UserManageServlet
 */
@WebServlet("/UserManageServlet")
public class UserManageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserManageServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//まずはnullだからif文に入らず50行目以降の処理を行う(最初だけ1ページ目を表示させる処理を行う)
		String id = null;
		String pg = null;

		try {
			UserDao userDao = DaoFactory.createUserDao();
			//jspから戻ってくるときにidとpgの値を持ってくるため2回目以降nullでなくなりif文に入る
			if (request.getParameter("id") != null & request.getParameter("pg") != null) {
				id = request.getParameter("id");
				pg = request.getParameter("pg");

				User user = new User();
				user.setPageCount(Integer.parseInt(pg));//pgをページ数としてセットする
				List<User> UserList = userDao.findAll(user);

				request.setAttribute("UserList", UserList);
				request.getRequestDispatcher("WEB-INF/view/userManage.jsp?id=" + id + "&page=" + pg).forward(request,
						response);

			} else {

				//行数を取得してページを振り分ける
				User user = userDao.Count();//最後の行を取得
				Integer r = user.getRow(); //行数を取得
				int f = r % 5; //行数を5で割った余り
				Integer p = 1; //ページ番号

				if (r < 6) { //5行までは1ページ目
					p = 1;
				} else if (f != 0) {
					p = (r - r % 5) / 5 + 1;//余りが出たとき、振り分けられるページ番号
				} else {
					p = r / 5; //余りがないときは行数÷5がページ番号
				}

				//1ページ目の表示
				user.setPageCount(1);
				List<User> UserList = userDao.findAll(user);

				request.setAttribute("UserList", UserList);
				request.getRequestDispatcher("WEB-INF/view/userManage.jsp?id=" + p + "&page=1").forward(request, response);
			}

		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
