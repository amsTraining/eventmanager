package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DaoFactory;
import dao.UserDao;
import domain.User;

/**
 * Servlet implementation class UserRegistServlet
 */
@WebServlet("/UserRegistServlet")
public class UserRegistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserRegistServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		int err = 0;
		request.setAttribute("err", err);
		request.getRequestDispatcher("WEB-INF/view/userRegist.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		UserDao GroupDao = DaoFactory.createUserDao();
		String name = request.getParameter("name");

		String login_id = request.getParameter("login_id");
		//エラー表示用の変数初期化
		int err = 0;
		request.setAttribute("err", err);
		try {
			//ユーザーのログインIDのリストをSQL文で持ってくる
			List<User> login_idList = GroupDao.listLogin_id();
			for (User loginList : login_idList) {
				login_id = request.getParameter("login_id");
				if (login_id.equals(loginList.getLogin_id())) {
					//System.out.println("ログインID登録失敗");
					err = -1;
					request.setAttribute("err", err);
					request.getRequestDispatcher("WEB-INF/view/userRegist.jsp").forward(request, response);
				}
			}
		} catch (Exception e3) {
			// TODO 自動生成された catch ブロック
			throw new ServletException(e3);
		}
		String login_pass;
		try {
			login_pass = GroupDao.makePasswordHash(request.getParameter("login_pass"));
		} catch (Exception e1) {
			// TODO 自動生成された catch ブロック
			throw new ServletException(e1);
		}
		Integer typeId = Integer.parseInt(request.getParameter("typeId"));

		String groupName = request.getParameter("groupName");
		Integer groupId = (Integer) request.getSession().getAttribute("loginGroupId");

		User group = new User();
		group.setGroupName(groupName);

		try {
			User id = GroupDao.findByGroupId(group);
			groupId = id.getGroupId();
		} catch (Exception e3) {
			// TODO 自動生成された catch ブロック
			e3.printStackTrace();
		}
		User user = new User();
		user.setName(name);
		user.setLogin_id(login_id);
		user.setPass(login_pass);
		user.setTypeId(typeId);
		user.setGroupId(groupId);

		try {
			UserDao UserDao = DaoFactory.createUserDao();
			UserDao.insert(user);
			request.getRequestDispatcher("WEB-INF/view/userRegistComp.jsp").forward(request, response);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

}
