package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AttendDao;
import dao.DaoFactory;
import dao.EventDao;
import domain.Attend;
import domain.Event;

/**
 * Servlet implementation class TodaysEventServlet
 */
@WebServlet({ "/TodaysEventServlet", "/todaysEvent" })
public class TodaysEventServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TodaysEventServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		String id = null;
		String pg = null;

		try {

			AttendDao AttendDao = DaoFactory.createAttendDao();
			//			String loginUserId = (String) request.getSession().getAttribute("loginId");
			Attend a = new Attend();
			String loginId = (String) request.getSession().getAttribute("loginId");
			a.setLoginId(loginId);
			List<Attend> attendEventList = AttendDao.findByLoginId(a);
			EventDao EventDao = DaoFactory.createEventDao();

			if (request.getParameter("id") != null & request.getParameter("pg") != null) {
				id = request.getParameter("id");
				pg = request.getParameter("pg");
				Event event = new Event();
				event.setPageCount(Integer.parseInt(pg));
				List<Event> EventList = EventDao.findByToday(event, attendEventList);

				request.setAttribute("eventList", EventList);
				request.getRequestDispatcher("WEB-INF/view/todaysEvent.jsp?id=" + id + "&page=" + pg).forward(request,
						response);

			} else {
				Event event = EventDao.CountToday();
				Integer r = event.getRow();
				int f = r % 5;
				Integer p = 1;
				if (r < 6) {
					p = 1;

				} else if (f != 0) {
					p = (r - r % 5) / 5 + 1;

				} else {
					p = r / 5;

				}
				event.setPageCount(1);
				List<Event> EventList = EventDao.findByToday(event, attendEventList);

				request.setAttribute("eventList", EventList);
				request.getRequestDispatcher("WEB-INF/view/todaysEvent.jsp?id=" + p + "&page=1").forward(request, response);
			}

		} catch (Exception e) {
			throw new ServletException(e);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

}
