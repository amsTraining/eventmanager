package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
/*
 * 説明	このプログラムはサーブレットから呼び出してください
 *
 * サーブレットに下記を追記することでサーブレットから本クラスを呼び出すことができます
 *
 *  ServletContext context = this.getServletContext();
 * 	String path = context.getRealPath(""/WEB-INF/classes/message_ja.properties"");
 * 	PropertiesGetter p = new PropertiesGetter();
 * 	p.getProperties(path);
 *
 *
 *
 *
 *
 * */

public class PropertiesGetter {

	public void getProperties(String path) {

		Path path1 = Paths.get(path);
		try (BufferedReader reader = Files.newBufferedReader(path1, StandardCharsets.UTF_8)) {
			Properties pr = new Properties();
			pr.load(reader);
			String adress = pr.getProperty("message.welcome");
			System.out.println(adress);
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}

}
