<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	request.setCharacterEncoding("UTF-8");
	String id = null;
	if (request.getParameter("id") != null) {//idに値が入っていればその値をidに持たせる
		id = request.getParameter("id");
	} else {
		id = "0";
	} ;
%>
<%
	String ac = null;
	if (request.getParameter("page") != null) {//pageに値が入っていればその値をacに持たせる
		ac = request.getParameter("page");
	} else {
		ac = "0";
	}
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="/Team03/view/css/style.css" rel="stylesheet">
<link href="/Team03/view/css/sticky-footer.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />
<title>イベント管理</title>
</head>
<body>
	<div class="wrapper container">
		<!-- ▼ ヘッダ -->
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div class="navbar-header">
					<!-- ▼ ハンバーガー ボタン -->
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#pageNavbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a href="#" class="navbar-brand">Event Manager</a>
				</div>
				<!-- ▼ グローバルナビ -->
				<div class="collapse navbar-collapse" id="pageNavbar">
					<ul class="nav navbar-nav">
						<li><a href="TodaysEventServlet">本日のイベント</a></li>
						<li class="active"><a href="EventManageServlet">イベント管理</a></li>
						<c:if test="${loginUserType==1}">
						<li><a href="UserManageServlet">ユーザ管理</a></li>
						</c:if>
					</ul>
					<ul class="nav navbar-nav navbar-right margin-navbar-right">
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown"> <span
								class="glyphicon glyphicon-user"></span> <c:out value="${loginUserName}" /> <span
								class="caret"></span>
						</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="LogoutServlet">ログアウト</a></li>
							</ul></li>
					</ul>
				</div>
			</nav>
		</header>
		<main class="main-contents col-sm-10 col-sm-offset-1">
		<article>
			<h1>イベント一覧</h1>

				<nav class="clearfix">
					<ul class="pagination pull-left">
						<li class="page-item"><a aria-level="前ページへ"
							href="EventManageServlet?id=<%=id%>
		&pg=<%if (Integer.parseInt(ac) <= 1) {%><%=1%><%} else {%><%=Integer.parseInt(ac) - 1%><%}%>"><span
								aria-hidden="true">≪</span></a></li>

						<%
							for (int i = 1; i <= Integer.parseInt(id); i++) {
								String u = "eventManage.jsp?id=" + id + "&page=" + Integer.toString(i);
								if (i == Integer.parseInt(ac)) {
						%>
						<li class="active">
							<%
								} else {
							%>

						<li>
							<%
								}
							%> <a href="EventManageServlet?id=<%=id%>&pg=<%=i%>"><%=i%></a>
						</li>

						<%
							}
						%><li><a aria-label="次のページへ"
							href="EventManageServlet?id=<%=id%>&pg=<%if (Integer.parseInt(ac) >= (Integer.parseInt(id))) {%><%=id%><%} else {%><%=Integer.parseInt(ac) + 1%><%}%>"><span
								aria-hidden="true">≫</span></a></li>
					</ul>
				</nav>
						<div class="table-responsive">
              <table class="table table-bordered">
                <thead>
							<tr>
								<th class="col-md-3">タイトル</th>
								<th class="col-md-4">開始日時</th>
								<th class="col-md-2">場所</th>
								<th class="col-md-2">対象グループ</th>
								<th class="col-md-1">詳細</th>
							</tr>
							</thead>
							<tbody>
							<c:forEach items="${EventList}" var="Event">
								<tr>
									<td class="col-md-3"><c:out value="${Event.title}" />
									<c:if test="${ Event.attendtf}"><span class="label label-danger">参加</span></c:if>
									</td>
									<td class="col-md-4"><fmt:parseDate
											value="${Event.start }" pattern="yyyy-MM-dd HH:mm:ss"
											var="start" /> <fmt:formatDate value="${start }"
											var="startFormat" pattern="yyyy年MM月dd日(E) HH時mm分" /> <c:out
											value="${startFormat}" /></td>
									<td class="col-md-2"><c:out value="${Event.place}" /></td>
									<td class="col-md-2"><c:out value="${Event.groupName}" /></td>
									<td class="col-md-1">
										<form action="EventDetail" method="get">
											<button type="submit" class="btn btn-default"
												value="${Event.eventId }" name="key">詳細</button>
										</form>
									</td>
								</tr>
							</c:forEach>
							</tbody>
						</table>
						</div>
					<form action="EventRegistServlet" method="get">
						<input type="submit" class="btn btn-primary" value="イベントの登録" />
					</form>

		       </article>
		     </main>
		</div>
	<!-- ▼ フッタ -->
	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>
	<!-- ▼ jQuery … CDNから取得 -->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"
		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		crossorigin="anonymous"></script>
	<!-- ▼ jQuery … jQueryの定義に失敗(CDNから読込めない?)した場合はローカルのファイルを読込む -->
	<script>
		if (!window.jQuery) {
			document.write('<script src="/Team03/view/js/jquery-3.3.1.min.js"><\/script>');
		}
	</script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
		<script src="/Team03/view/js/jquery-3.3.1.min.js"></script>
<script src="/Team03/view/js/dropdown.js"></script>
</body>
</html>