<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
    <jsp:useBean id="start" class="java.util.Date"/>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>イベント管理</title>
    <!-- ▼ Internet Explorer … 表示モード-＞常に標準モード -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 -->
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- ▼ Bootstrap -->
<link href="/Team03/view/css/style.css" rel="stylesheet">
<link href="/Team03/view/css/sticky-footer.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />
<script src="/Team03/view/js/jquery-3.3.1.min.js"></script>

<script>
 $(document).ready(function(){
	 $('form').submit(function(){
		var title=$('input[name=title]').val();
		var start=$('input[name=start]').val();
		var end=$('input[name=end]').val();
		var place=$('input[name=place]').val();
		if(title==''){
			alert('タイトルを入力してください');
			return false;
		}
		if(start==''){
			alert('開始日時を入力してください');
			return false;
		}
		if(end==''){
			alert('終了日時を入力してください');
			return false;
		}
		if(place==''){
			alert('場所を入力してください');
			return false;
		}
	});
});
 </script>
</head>
<body>
   <div class="wrapper container">
      <!-- ▼ ヘッダ -->
      <header>
        <nav class="navbar navbar-default navbar-fixed-top">
          <div class="navbar-header">
            <!-- ▼ ハンバーガー ボタン -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#pageNavbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand">Event Manager</a>
          </div>
          <!-- ▼ グローバルナビ -->
          <div class="collapse navbar-collapse" id="pageNavbar">
            <ul class="nav navbar-nav">
              <li>
                <a href="TodaysEventServlet">本日のイベント</a>
              </li>
              <li class="active">
                <a href="EventManageServlet">イベント管理</a>
              </li>
              <c:if test="${loginUserType==1}">
              <li>
                <a href="UserManageServlet">ユーザ管理</a>
              </li>
              </c:if>
            </ul>
            <ul class="nav navbar-nav navbar-right margin-navbar-right">
              <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown">
                  <span class="glyphicon glyphicon-user"></span>
                  <c:out value="${loginUserName}"/>
                  <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="LogoutServlet">ログアウト</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>

     <!-- ▼ メインコンテンツ -->
      <main class="main-contents col-sm-10 col-sm-offset-1">
        <article>
          <h2>イベント編集</h2>
          <!-- ▼ 入力フォーム -->
          <form action="eventEdit" method="post">
            <!-- ▼ 情報表示 -->
<p>
<input type="hidden" name="eventId" value="${event.eventId }">
</p>
            <label>タイトル (必須)</label>
            <p>
              <input type="text" name="title" class="form-control" value="${event.title}"/>
            </p>
            <label>開始日時 (必須)</label>
            <p>
             <fmt:formatDate value="${event.start}" var="startFormat" pattern="yyyy-MM-dd HH:mm"/>
              <input type="text" name="start" class="form-control" value="${startFormat}"/>
              <%
					int	err = (Integer) request.getAttribute("err");
					if (err == -1) {
				%>
				<p class="text-danger">開始日時に過去の時間が設定されています。</p>
				<%}else if(err == -3){%>
				<p class="text-danger">正しい日付を入力してください</p>
				<%} %>
            <label>終了日時</label>
            <p>
             <fmt:formatDate value="${event.end}" var="endFormat" pattern="yyyy-MM-dd HH:mm"/>
              <input type="text" name="end" class="form-control" value="${endFormat }"/>
              <%
					int err2 = (Integer) request.getAttribute("err2");
					if (err2 == -1) {
				%>
				<p class="text-danger">終了日時に過去の時間が設定されています。</p>
			    <%
					}else if(err2 == -2){
				%>
				<p class="text-danger">開始時間より前の時間が設定されています。</p>
				<%}else if(err2 == -3){%>
				<p class="text-danger">正しい日付を入力してください</p>
				<%} %>
            <label>場所 (必須)</label>
            <p>
              <input type="text" name="place" class="form-control" value="${event.place }"/>
            </p>
            <label>対象グループ</label>
            <div class="input-group choose-group js-choose-group" >
              <input type="text" readonly name="groupName" class="form-control js-group-input" value="${event.groupName}"/>
              <span class="input-group-btn">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu pull-right">
                  <li><a class="js-group-name"  >全員</a></li>
                  <li><a class="js-group-name" >技術部</a></li>
                  <li><a class="js-group-name" >営業部</a></li>
                  <li><a class="js-group-name" >総務部</a></li>
                  <li><a class="js-group-name" >人事部</a></li>
                </ul>
              </span>
            </div>
            <label>詳細</label>
            <p>
              <textarea name="detail" class="form-control textarea-width"  value="${event.detail}"></textarea>
            </p>

            <a href="EventDetail?key=${event.eventId }" class="btn btn-default" >キャンセル</a>
            <button type="submit" name="" value="" class="btn btn-primary">保存</button>
<p>
<input type="hidden" name="registeredById" value="${event.registeredById }">
</p>
          </form>
        </article>
      </main>
    </div>


    <!-- ▼ フッタ -->
    <footer class="footer">
      <p class="text-center text-muted">© icloud 2018 Inc.</p>
    </footer>
    <!-- ▼ jQuery … CDNから取得 -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
      integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <!-- ▼ jQuery … jQueryの定義に失敗(CDNから読込めない?)した場合はローカルのファイルを読込む -->
    <script>if (!window.jQuery){ document.write('<script src="/Team03/view/js/jquery-3.3.1.min.js"><\/script>'); }</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="/Team03/view/js/jquery-3.3.1.min.js"></script>
<script src="/Team03/view/js/dropdown.js"></script>
</body>
</html>