﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
<!-- ▼ Internet Explorer … 表示モード-＞常に標準モード -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- ▼ Bootstrap -->
<link href="/Team03/view/css/style.css" rel="stylesheet">
<link href="/Team03/view/css/sticky-footer.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />
    <title>ユーザー管理</title>
    <script src="/Team03/view/js/jquery-3.3.1.min.js"></script>
<script>
$(document).ready(function(){
	 $('form').submit(function(){
		 var login_id=$('input[name=login_id]').val();
		 var login_pass=$('input[name=login_pass]').val();
		 var groupName=$('input[name=groupName]').val();
		 var name=$('input[name=name]').val();
		 if(name==''){
			 alert('名前を入力してください');
			 return false;
		 }
		 if(login_id==''){
			 alert('ログインIDを入力してください');
			 return false;
		 }
		 if(login_pass==''){
			 alert('パスワードを入力してください');
			 return false;
		 }
		 if(groupName==''){
			 alert('所属グループを選択してください');
			 return false;
		 }

	 });
});
</script>
<body>
	<div class="wrapper container">
		<!-- ▼ ヘッダ -->
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div class="navbar-header">
					<!-- ▼ ハンバーガー ボタン -->
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#pageNavbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a href="#" class="navbar-brand">Event Manager</a>
				</div>
				<!-- ▼ グローバルナビ -->
				<div class="collapse navbar-collapse" id="pageNavbar">
					<ul class="nav navbar-nav">
						<li><a href="TodaysEventServlet">本日のイベント</a></li>
						<li><a href="EventManageServlet">イベント管理</a></li>
						<li class="active"><a href="UserManageServlet">ユーザ管理</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right margin-navbar-right">
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown"> <span
								class="glyphicon glyphicon-user"></span> <c:out
									value="${loginUserName}" /> <span class="caret"></span>
						</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="LogoutServlet">ログアウト</a></li>
							</ul></li>
					</ul>
				</div>
			</nav>
		</header>
		<!-- ▼ メインコンテンツ -->
		<main class="main-contents col-sm-10 col-sm-offset-1">
		<article>
			<h2>ユーザ登録</h2>
			<!-- ▼ 入力フォーム -->
			<form action="UserRegistServlet" method="post">
				<!-- ▼ 情報表示 -->
				<label>氏名 (必須)</label>
				<p>
					<input type="text" name="name" class="form-control"
						placeholder="氏名" />
				</p>

				<label>ログインID (必須)</label>
				<p>
				<%
					int err = (Integer) request.getAttribute("err");
					if (err == -1) {
				%>
				<p class="text-danger">すで登録済みのログインIDのため登録ができません。</p>
				<%
					}
				%>
				<input type="text" name="login_id" class="form-control"
						placeholder="ログインID" />
				</p>
				<label>パスワード (必須)</label>
				<p>
					<input type="password" name="login_pass" class="form-control"
						placeholder="パスワード" />
				</p>
				<label>所属グループ (必須)</label>
				<div class="input-group choose-group js-choose-group">
					<input type="text" readonly="readonly" name="groupName"
						class="form-control js-group-input" placeholder="所属グループ" /> <span
						class="input-group-btn">
						<button class="btn btn-default dropdown-toggle"
							data-toggle="dropdown">
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu pull-right">
							<li><a class="js-group-name">技術部</a></li>
							<li><a class="js-group-name">営業部</a></li>
							<li><a class="js-group-name">総務部</a></li>
							<li><a class="js-group-name">人事部</a></li>
						</ul>
					</span>
				</div>
				<div class="form-group row form-check">
					<p>ユーザー権限</p>
					<input type="radio" name="typeId" value="2" checked="checked" />一般ユーザー
					<input type="radio" name="typeId" value="1" />管理者
				</div>

				<!-- ▼ ユーザの登録キャンセルボタン -->
				<!--
            <button type="submit" name="" value="" class="btn btn-default">キャンセル</button>
            -->
				<a href="UserManageServlet" class="btn btn-default">キャンセル</a>
				<!-- ▼ ユーザの登録ボタン -->
				<!--
            <button type="submit" name="" value="" class="btn btn-primary">登録</button>
            -->
				<input type="submit" value="登録" class="btn btn-primary">
			</form>
		</article>
		</main>
	</div>
	<!-- ▼ フッタ -->
	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>
	<!-- ▼ jQuery … CDNから取得 -->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"
		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		crossorigin="anonymous"></script>
	<!-- ▼ jQuery … jQueryの定義に失敗(CDNから読込めない?)した場合はローカルのファイルを読込む -->
	<script>if (!window.jQuery){ document.write('<script src="/Team03/view/js/jquery-3.3.1.min.js"><\/script>'); }</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="/Team03/view/js/jquery-3.3.1.min.js"></script>
<script src="/Team03/view/js/dropdown.js"></script>
</body>
</html>
