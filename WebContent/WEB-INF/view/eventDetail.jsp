<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
<!-- ▼ Internet Explorer … 表示モード-＞常に標準モード -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- ▼ Bootstrap -->
<link href="/Team03/view/css/style.css" rel="stylesheet">
<link href="/Team03/view/css/sticky-footer.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />
<title>イベント管理</title>
</head>
<body>
	<div class="wrapper container">
		<!-- ▼ ヘッダ -->
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div class="navbar-header">
					<!-- ▼ ハンバーガー ボタン -->
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#pageNavbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a href="#" class="navbar-brand">Event Manager</a>
				</div>
				<!-- ▼ グローバルナビ -->
				<div class="collapse navbar-collapse" id="pageNavbar">
					<ul class="nav navbar-nav">
						<li><a href="TodaysEventServlet">本日のイベント</a></li>
						<li class="active"><a href="EventManageServlet">イベント管理</a></li>
						<c:if test="${loginUserType==1}">
						<li><a href="UserManageServlet">ユーザ管理</a></li>
						</c:if>
					</ul>
					<ul class="nav navbar-nav navbar-right margin-navbar-right">
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown"> <span
								class="glyphicon glyphicon-user"></span> <c:out value="${loginUserName}" /> <span
								class="caret"></span>
						</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="LogoutServlet">ログアウト</a></li>
							</ul></li>
					</ul>
				</div>
			</nav>
		</header>
		<!-- ▼ メインコンテンツ -->
		<main class="main-contents col-sm-10 col-sm-offset-1">
		<article>
			<h2>イベント詳細</h2>
			<!-- ▼ 入力フォーム -->

			<!-- ▼ 情報表示 -->
			<div class="table-responsive">
				<table class="table">
					<tr>
						<th>タイトル</th>
						<td><c:out value="${event.title}" /> <!-- ▼ 参加ラベル (※自分が参加するイベントの場合) -->
							<c:if test="${join}">
								<span class="label label-danger">参加</span>
							</c:if></td>

					</tr>
					<tr>
						<th>開始日時</th>
						<td><fmt:parseDate value="${event.start }"
								pattern="yyyy-MM-dd HH:mm:ss" var="start" /> <fmt:formatDate
								value="${start }" var="startFormat"
								pattern="yyyy年MM月dd日(E) HH時mm分" /> <c:out
								value="${startFormat}" /></td>
					</tr>
					<tr>
						<th>終了日時</th>
						<td><fmt:parseDate value="${event.end }"
								pattern="yyyy-MM-dd HH:mm:ss" var="start" /> <fmt:formatDate
								value="${start }" var="startFormat"
								pattern="yyyy年MM月dd日(E) HH時mm分" /> <c:out
								value="${startFormat}" /></td>
					</tr>
					<tr>
						<th>場所</th>
						<td><c:out value="${event.place}" /></td>
					</tr>
					<tr>
						<th>対象グループ</th>
						<td><c:out value="${event.groupName}" /></td>
					</tr>
					<tr>
						<th>詳細</th>
						<td><c:out value="${event.detail}" /></td>
					</tr>
					<tr>
						<th>登録者</th>
						<td><c:out value="${event.registeredByName}" /></td>
					</tr>
					<tr>
						<th>参加者</th>
                           <td><c:forEach items="${eventList}" var="event" varStatus="status">
	                           <c:out value="${event.attendName}" />
	                           <c:if test="${!status.last}">,
	                           </c:if>
</c:forEach></td>
					</tr>
				</table>
			</div>
			<!-- ▼ 一覧に戻るボタン -->
			<div style ="display:inline-flex">
				<form action="EventManageServlet" method="get">
					<button type="submit" name="eventId" value="${eventId}" class="btn btn-primary">一覧に戻る</button>
				</form>
				&nbsp;
				<!-- ▼ 参加するボタン (※自分が参加しないイベントの場合) -->
				<c:if test="${ifbefore}">
<c:if test="${event.groupId==loginGroupId||event.groupName=='全員'}">
				<c:if test="${!join}">
				<form action="EventDetailInsert" method="post">
					<button type="submit" name="eventId" value="${eventId}" class="btn btn-info">参加する</button>
					</form>
					&nbsp;
				</c:if>
				<!-- ▼ 参加を取り消すボタン (※自分が参加するイベントの場合) -->
				<c:if test="${join}">
				<form action="EventDetailDelete" method="post">
					<button type="submit" name="eventId" value="${eventId}" class="btn btn-warning">参加を取り消す</button>
				</form>
				&nbsp;
				</c:if>
				</c:if>
				</c:if>
				<!-- ▼ 編集ボタン (※自分が登録したイベント／管理ユーザの場合) -->
				<!--
            <button type="submit" name="" value="" class="btn btn-default">編集</button>
            -->
				<c:if test="${loginUserType ==1 || (loginUserId == event.registeredById&&ifbefore)}">
				<form action="EventEditServlet">
					<button type="submit" name="userId" value="${userId}" class="btn btn-default">編集</button>
					<input type="hidden" name="eventId" value="${eventId}" class="btn btn-default"/>
					</form>&nbsp;
					<!-- ▼ 削除ボタン (※自分が登録したイベント／管理ユーザの場合) -->
					<form action="EventDeleteComp" method="post">
						<button type="button" class="btn btn-danger" data-toggle="modal"
							data-target="#confirmModal">削除</button>
						<!-- ▼ モーダル・ダイアログ -->
						<div class="modal fade" id="confirmModal" tabindex="-1">
							<div class="modal-dialog" style="z-index: 1500">
								<div class="modal-content">
									<div class="modal-body">
										<button type="button" class="close" data-dismiss="modal">
											<span>×</span>
										</button>
										<p>本当に削除してよろしいですか？</p>
									</div>
									<div class="modal-footer">
										<button type="button" name="" value=""

											class="btn btn-default" data-dismiss="modal">Cancel</button>

										<button type="submit" name="eventId" value="${eventId}" class="btn btn-primary">OK</button>

									</div>
								</div>
							</div>
						</div>
					</form>
				</c:if>
			</div>

		</article>
		</main>
	</div>
	<!-- ▼ フッタ -->
	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>
	<!-- ▼ jQuery … CDNから取得 -->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"
		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		crossorigin="anonymous"></script>
	<!-- ▼ jQuery … jQueryの定義に失敗(CDNから読込めない?)した場合はローカルのファイルを読込む -->
	<script>
		if (!window.jQuery) {
			document.write('<script src="/Team03/view/js/jquery-3.3.1.min.js"><\/script>');
		}
	</script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
		<script src="/Team03/view/js/jquery-3.3.1.min.js"></script>
<script src="/Team03/view/js/dropdown.js"></script>
</body>
</html>
