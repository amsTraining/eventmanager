﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<!-- ▼ Internet Explorer … 表示モード-＞常に標準モード -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- ▼ Bootstrap -->
<link href="/Team03/view/css/style.css" rel="stylesheet" />
<link href="/Team03/view/css/sticky-footer.css" rel="stylesheet" />
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
	rel="stylesheet" />
<title>イベント管理</title>

</head>
<body>
	<div class="wrapper container">
		<!-- ▼ ヘッダ -->
		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div class="navbar-header">
					<!-- ▼ ハンバーガー ボタン -->
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#pageNavbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a href="#" class="navbar-brand">Event Manager</a>
				</div>
				<!-- ▼ グローバルナビ -->
				<div class="collapse navbar-collapse" id="pageNavbar">
					<ul class="nav navbar-nav">
						<li><a href="TodaysEventServlet">本日のイベント</a></li>
						<li><a href="EventManageServlet">イベント管理</a></li>
						<c:if test="${loginUserType==1}">
							<li class="active"><a href="UserManageServlet">ユーザ管理</a></li>
						</c:if>
					</ul>
					<ul class="nav navbar-nav navbar-right margin-navbar-right">
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown"> <span
								class="glyphicon glyphicon-user"></span> <c:out
									value="${loginUserName}" /> <span class="caret"></span>
						</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="LogoutServlet">ログアウト</a></li>
							</ul></li>
					</ul>
				</div>
			</nav>
		</header>
		<!-- ▼ メインコンテンツ -->
		<main class="main-contents col-sm-10 col-sm-offset-1">
		<article>
			<h2>ユーザ詳細</h2>
			<!-- ▼ 入力フォーム -->
			<!--<form action="/Team03_EventManager/UserEditServlet" method="post">-->
			<!-- ▼ 情報表示 -->
			<div class="table-responsive">
				<table class="table">

					<tr>
						<th>ID</th>
						<td><c:set var="a" value="${user.userId}" /> <c:out
								value="${a} " /></td>
					</tr>
					<tr>
						<th>氏名</th>
						<td><c:out value="${user.name}" /></td>
					</tr>
					<tr>
						<!-- user_typesテーブルの値-->
						<th>所属グループ</th>
						<td><c:out value="${ user.groupName}" /></td>
					</tr>

				</table>
			</div>
			<!-- ▼ 一覧に戻るボタン -->
			<!--
            <button type="submit" name="" value="" class="btn btn-primary">一覧に戻る</button>
            -->
			<div>
				<a href="UserManageServlet" class="btn btn-primary">一覧に戻る</a>
				<!-- ▼ 編集ボタン -->
				<a
					href="<c:url value="/UserEditServlet" >
				<c:param name="userId" value="${ user.userId}" /></c:url>"
					class="btn btn-default">編集</a>
				<!--<button type="submit" name="userId" value="${user.userId}"class="btn btn-default">編集</button>-->
				<!-- ▼ 削除ボタン -->
				<c:if test="${user.typeId!=1}">
					<button type="button" class="btn btn-danger" data-toggle="modal"
						data-target="#confirmModal">削除</button>
					<!-- ▼ モーダル・ダイアログ -->
					<div class="modal fade" id="confirmModal" tabindex="-1">
						<div class="modal-dialog" style="z-index: 1500">
							<div class="modal-content">
								<div class="modal-body">
									<button type="button" class="close" data-dismiss="modal">
										<span>×</span>
									</button>
									<p>本当に削除してよろしいですか？</p>
									<p class="text-danger">このユーザーを削除するとこのユーザーが登録したイベントはすべて削除されます</p>
								</div>
								<div class="modal-footer">
									<form action="UserDeleteServlet" method="post">
										<button type="button" class="btn btn-default"
											data-dismiss="modal">Cancel</button>

										<button type="submit" name="userId" value="${user.userId}"
											class="btn btn-primary">OK</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</c:if>
			</div>

		</article>

		</main>
	</div>
	<!-- ▼ フッタ -->
	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>
	<!-- ▼ jQuery … CDNから取得 -->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"
		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		crossorigin="anonymous"></script>
	<!-- ▼ jQuery … jQueryの定義に失敗(CDNから読込めない?)した場合はローカルのファイルを読込む -->
	<script>
		if (!window.jQuery) {
			document.write('<script src="/Team03/view/js/jquery-3.3.1.min.js"><\/script>');
		}
	</script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
		<script src="/Team03/view/js/jquery-3.3.1.min.js"></script>
<script src="/Team03/view/js/dropdown.js"></script>
</body>
</html>