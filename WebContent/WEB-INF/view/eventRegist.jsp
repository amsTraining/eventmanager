﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
<!-- ▼ Internet Explorer … 表示モード-＞常に標準モード -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- ▼ デバイス(スマホ)対応 … 幅-＞端末の画面幅に合わせる ピクセル比率-＞CSSの記述と端末が1：1 -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- ▼ Bootstrap -->
<link href="/Team03/view/css/style.css" rel="stylesheet">
<link href="/Team03/view/css/sticky-footer.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />
    <title>イベント管理</title>

<script>
	$(document).ready(function() {
		$('form').submit(function() {
			var title = $('input[name=title]').val();
			var start = $('input[name=start]').val();
			var end = $('input[name=end]').val();
			var place = $('input[name=place]').val();
			if (title == '') {
				alert('タイトルを入力してください');
				return false;
			}
			if (start == '') {
				alert('開始日時を入力してください');
				return false;
			}
			if (end == '') {
				alert('終了日時を入力してください');
				return false;
			}
			if (place == '') {
				alert('場所を入力してください');
				return false;
			}
			if (start == '') {
				alert('開始日時を入力してください');
				return false;
			}
			if (end == '') {
				alert('開始日時を入力してください');
				return false;
			}
		});
	});
</script>
</head>
<body>
	<div class="wrapper container">
		<!-- ▼ ヘッダ -->
		 <header>
        <nav class="navbar navbar-default navbar-fixed-top">
          <div class="navbar-header">
            <!-- ▼ ハンバーガー ボタン -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#pageNavbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand">Event Manager</a>
          </div>
          <!-- ▼ グローバルナビ -->
          <div class="collapse navbar-collapse" id="pageNavbar">
            <ul class="nav navbar-nav">
              <li>
                <a href="TodaysEventServlet">本日のイベント</a>
              </li>
              <li  class="active">
                <a href="EventManageServlet">イベント管理</a>
              </li>
              <c:if test="${loginUserType==1}">
              <li>
                <a href="UserRegistServlet">ユーザ管理</a>
              </li>
              </c:if>
            </ul>
            <ul class="nav navbar-nav navbar-right margin-navbar-left">
              <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown">
                  <span class="glyphicon glyphicon-user"></span>
                 <c:out value="${loginUserName}" />
                  <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="LogoutServlet">ログアウト</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
		<!-- ▼ メインコンテンツ -->
		<main class="main-contents col-sm-10 col-sm-offset-1">
		<article>
			<h2>イベント登録</h2>
			<!-- ▼ 入力フォーム -->
			<form action="EventRegistServlet" method="post">
				<!-- ▼ 情報表示 -->
				<label>タイトル (必須)</label>
				<p>
					<input type="text" name="title" class="form-control" placeholder="" />
				</p>

				<label>開始日時 (必須)</label>
				<p>
					<input type="text" name="start" class="form-control"
						placeholder="2018-01-01 00:00" />
				</p>
				<%
					int	err = (Integer) request.getAttribute("err");
					if (err == -1) {
				%>
				<p class="text-danger">開始日時に過去の時間が設定されています。</p>
				<%}else if(err == -3){%>
				<p class="text-danger">正しい日付を入力してください</p>
				<%} %>

				<label>終了日時 (必須)</label>
				<p>
					<input type="text" name="end" class="form-control"
						placeholder="2018-01-01 00:00" />
				</p>
				<%
					int err2 = (Integer) request.getAttribute("err2");
					if (err2 == -1) {
				%>
				<p class="text-danger">終了日時に過去の時間が設定されています。</p>
				<%
					}else if(err2 == -2){
				%>
				<p class="text-danger">開始時間より前の時間が設定されています。</p>
				<%}else if(err2 == -3){%>
				<p class="text-danger">正しい日付を入力してください</p>
				<%} %>
				<label>場所 (必須)</label>
				<p>
					<input type="text" name="place" class="form-control" placeholder="" />
				</p>
				<label>対象グループ</label>
				<div class="input-group choose-group js-choose-group">
					<input type="text" readonly="readonly" name="groupName"
						class="form-control js-group-input" placeholder="全員" /> <span
						class="input-group-btn">
						<button class="btn btn-default dropdown-toggle"
							data-toggle="dropdown">
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu pull-right">
							 <li><a class="js-group-name"  >全員</a></li>
                             <li><a class="js-group-name" >技術部</a></li>
                             <li><a class="js-group-name" >営業部</a></li>
                             <li><a class="js-group-name" >総務部</a></li>
                             <li><a class="js-group-name" >人事部</a></li>
						</ul>
					</span>
				</div>
				<label>詳細</label>
				<p>
					<textarea name="detail" class="form-control textarea-width"></textarea>
				</p>
				<!-- ▼ イベントの登録キャンセルボタン -->
				<!--
            <button type="submit" name="" value="" class="btn btn-default">キャンセル</button>
            -->
				<a href="EventManageServlet" class="btn btn-default">キャンセル</a>
				<!-- ▼ イベントの登録ボタン -->
				<input type="submit" value="登録" class="btn btn-primary">
			</form>
		</article>
		</main>
	</div>
	<!-- ▼ フッタ -->
	<footer class="footer">
		<p class="text-center text-muted">© icloud 2018 Inc.</p>
	</footer>
	<!-- ▼ jQuery … CDNから取得 -->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"
		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		crossorigin="anonymous"></script>
	<!-- ▼ jQuery … jQueryの定義に失敗込めない?)した場合はローカルのファイルを読込む -->
	<script>
		if (!window.jQuery) {
			document.write('<script src="/Team03/view/js/jquery-3.3.1.min.js"><\/script>');
		}
	</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
   <script src="/Team03/view/js/jquery-3.3.1.min.js"></script>
    <script src="/Team03/view/js/dropdown.js"></script>
</body>
</html>
