<%@ page language="java"
contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%--
ページディレクティブ
属性：language、contentType、pageEncoding
--%>

<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>


<link href="/Team03/view/css/style.css" rel="stylesheet">
<link href="/Team03/view/css/sticky-footer.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />
<title>ログイン</title>

</head>

<body class="login-body">
    <div class="wrapper login-wrapper container">


<main class="login-contents col-sm-8 col-sm-offset-2 col-xs-12">

<article>
<div class="panel login-panel panel-default">

<div class="panel-heading">
 Event Manager
 </div>

<div class="panel-body">
<form action="LoginServlet" method="post">

<p>
<%
boolean err = (boolean)request.getAttribute("error");
if(err == true){
%>
<%--★★ID、パスワードが誤っていた際の注意文★★--%>
<p class=warn>ログインIDかパスワードが誤っています</p>
<%} %>
<p>
<input type="text" name="loginId" class="form-control" placeholder="ログインID"/>
</p>
<p>
<input type="password" name="loginPass" class="form-control" placeholder="パスワード"/>
</p>

<%--
ユーザーの入力受付input要素のtextタイプ　Part2（p39,40）
--%>

<button type="submit" class="btn btn-block btn-primary">ログイン</button>

<%--
ログインボタンPart2（p146,147）
--%>

</form>
</div>

</div>
</article>

</main>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
      integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
 <script>
		if (!window.jQuery) {
			document.write('<script src="/Team03/view/js/jquery-3.3.1.min.js"><\/script>');
		}
</script>

<%--★★id,ログインのどちらかもしくは両方空欄の場合、
マウスのカーソルをログインに合わせたとき、アラートが出る
【パターン①】id、パターンのどちらが空欄でも同じアラート★★--%>
<script>
<%--
$(document).ready(function(){
	$('button').mouseover(function(){
		var loginId=$('input[name=loginId]').val();
		var loginPass=$('input[name=loginPass]').val();
		if(loginId=='' || loginPass==''){
			alert('ログインIDもしくはパスワードが入力されていません');
			return false;
		}

	});
});
--%>
<%--★★id,ログインのどちらかもしくは両方空欄の場合、
マウスのカーソルをログインに合わせたとき、アラートが出る
【パターン②】id、パターンの空欄状況によってアラートが変化★★--%>

<%--$(document).ready(function(){
	$('button').mouseover(function(){
		var loginId=$('input[name=loginId]').val();
		var loginPass=$('input[name=loginPass]').val();
		if(loginId==''&&loginPass==''){
			alert('ログインIDとパスワードが入力されていません');
			return false;
		}
		if(loginId==''){
			alert('ログインIDが入力されていません');
			return false;
		}
		if(loginPass==''){
			alert('パスワードが入力されていません');
			return false;
		}

	});
});
--%>
<%--★★カーソルを合わせたときだと、たまたまログインボタンにカーソルが
合わさった時でもアラートでてイライラするか？？？？？？と思ったので
【パターン③】パターン①のクリックver★★---%>
<%--
$(document).ready(function(){
	$('button').click(function(){
		var loginId=$('input[name=loginId]').val();
		var loginPass=$('input[name=loginPass]').val();
		if(loginId=='' || loginPass==''){
			alert('ログインIDもしくはパスワードが入力されていません');
			return false;
		}

	});
});
--%>

<%--★★カーソルを合わせたときだと、たまたまログインボタンにカーソルが
合わさった時でもアラートでてイライラするか？？？？？？と思ったので
【パターン④】パターン②のクリックver★★---%>

$(document).ready(function(){
	$('button').click(function(){
		var loginId=$('input[name=loginId]').val();
		var loginPass=$('input[name=loginPass]').val();
		if(loginId==''&&loginPass==''){
			alert('ログインIDとパスワードが入力されていません');
			return false;
		}
		if(loginId==''){
			alert('ログインIDが入力されていません');
			return false;
		}
		if(loginPass==''){
			alert('パスワードが入力されていません');
			return false;
		}
	});
});

<%--★★ID、パスワードが誤っていた際の注意文の色★★--%>
$(document).ready(function(){
$(".warn").css("color","red");
});



</script>
<script src="/Team03/view/js/jquery-3.3.1.min.js"></script>
</body>

</html>