-- MySQL dump 10.13  Distrib 5.7.22, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: eventdb
-- ------------------------------------------------------
-- Server version	5.7.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `place` varchar(255) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `detail` text,
  `registered_by` int(11) DEFAULT NULL,
  `CREATED` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,'勉強会（Java）','2018-05-29 11:30:15','2018-05-31 11:30:15','第一会議室',2,'データベースとの連携について行います。',1,'2018-05-14 13:09:20'),(2,'お食事会','2018-05-28 13:09:15','2018-05-31 14:09:15','赤坂',8,'一人3500円です。詳細は後日アナウンスします。',1,'2018-05-14 13:09:20'),(3,'HTML5勉強会','2018-05-29 11:30:15','2018-05-30 11:30:15','第二会議室',3,'対象はJavaScriptの基本的なコーディングが出来る方です。',2,'2018-05-29 11:30:15'),(4,'営業部勉強会','2018-05-29 11:30:15','2018-05-30 11:30:15','第二会議室',1,'なるべく参加してください。',1,'2018-05-14 13:09:20'),(5,'サッカー部練習','2018-05-29 11:30:15','2018-05-29 11:30:15','森林公園',8,'練習後交流会実施予定。',3,'2018-05-29 11:30:15'),(6,'新人懇親会','2018-05-29 11:30:15','2018-05-29 11:30:15','渋谷',8,'一人3500円です。詳細は後日アナウンスします。',4,'2018-05-29 11:30:15'),(7,'他部署交流会','2018-05-30 11:30:15','2018-05-29 11:30:15','渋谷',8,'一人3500円です。詳細は後日アナウンスします。',2,'2018-05-29 11:30:15'),(8,'BBQ懇親会','2018-05-31 11:30:15','2018-05-29 11:30:15','葛西臨海公園',8,'一人3500円です。詳細は後日アナウンスします。',1,'2018-05-29 11:30:15'),(9,'総務部懇親会','2018-05-29 11:30:15','2018-05-29 11:30:15','神楽坂',4,'一人3500円です。詳細は後日アナウンスします。',1,'2018-05-29 11:30:15'),(10,'広報部懇親会','2018-05-29 11:30:15','2018-05-29 11:30:15','神楽坂',5,'一人3500円です。詳細は後日アナウンスします。',1,'2018-05-29 11:30:15'),(11,'コーヒー試飲会','2018-05-28 11:30:15','2018-05-29 11:30:15','第一会議室',1,'詳細は後日アナウンスします。',1,'2018-05-29 11:30:15'),(12,'日帰りキャンプ','2018-05-25 11:30:15','2018-05-29 11:30:15','河口湖',1,'一人3500円です。詳細は後日アナウンスします。',1,'2018-05-29 11:30:15'),(13,'イベントテスト','2018-06-17 17:00:00','2018-06-17 21:00:00','パシフィコ横浜',7,'田村ゆかりFCイベント＠パシフィコ横浜',11,'2018-06-01 16:33:17');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-04 12:21:47
